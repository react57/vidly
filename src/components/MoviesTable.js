import * as React from "react";
import Table from "./common/Table";
import Button from "./common/Button";

class MoviesTable extends React.Component {
  columns = [
    { label: "Title", path: "title" },
    { label: "Genre", path: "genre.name" },
    { label: "Stock", path: "numberInStock" },
    { label: "Rate", path: "dailyRentalRate" },
    {
      key: "",
      content: (item) => (
        <Button
          label="delete"
          onClick={() => this.props.onDelete(item._id)}
          className="btn btn-sm btn-danger"
        />
      ),
    },
  ];

  render() {
    const { movieCount, movies, sortColumn, onSort } = this.props;

    return (
      <>
        <p>
          Showing {movies.length} of {movieCount} total movies.
        </p>
        <Table
          columns={this.columns}
          sortColumn={sortColumn}
          items={movies}
          itemsCount={movieCount}
          onSort={onSort}
        />
      </>
    );
  }
}

export default MoviesTable;
