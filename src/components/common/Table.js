import * as React from "react";
import PropTypes from "prop-types";
import TableHeader from "./TableHeader";
import TableBody from "./TableBody";

const Table = ({ columns, sortColumn, onSort, items, itemsCount }) => {
  return (
    <table className="table table-sm table-bordered table-hover">
      <caption>List of movies</caption>
      <TableHeader columns={columns} sortColumn={sortColumn} onSort={onSort} />
      <TableBody itemsCount={itemsCount} items={items} columns={columns} />
    </table>
  );
};

Table.propTypes = {
  columns: PropTypes.array.isRequired,
  sortColumn: PropTypes.object.isRequired,
  items: PropTypes.array.isRequired,
  itemsCount: PropTypes.number.isRequired,
  onSort: PropTypes.func.isRequired,
};

export default Table;
