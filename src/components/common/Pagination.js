import * as React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

const Pagination = ({
  totalItemsCount,
  itemsShowingCount,
  pageSize,
  currentPage,
  onPageChange,
}) => {
  if (itemsShowingCount === 0 && currentPage !== 1) {
    onPageChange(currentPage - 1);
  }

  const pagesCount = Math.ceil(totalItemsCount / pageSize);

  if (pagesCount === 1) return null;

  const pages = _.range(1, pagesCount + 1);

  return (
    <nav>
      <ul className="pagination">
        {pages.map((page) => (
          <li
            key={page}
            className={page === currentPage ? "active page-item" : "page-item"}
          >
            <a onClick={() => onPageChange(page)} className="page-link">
              {page}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

Pagination.propTypes = {
  totalItemsCount: PropTypes.number.isRequired,
  itemsShowingCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

export default Pagination;
