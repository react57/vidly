import * as React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

const TableBody = ({ itemsCount, items, columns }) => {
  const renderCell = (item, column) => {
    return column.content ? column.content(item) : _.get(item, column.path);
  };

  const createKey = (item, column) => {
    return item._id + (column.path || column.key);
  };

  return (
    <tbody>
      {itemsCount === 0 ? (
        <tr>
          <td className="text-center" colSpan={100}>
            No movies found
          </td>
        </tr>
      ) : (
        items.map((item) => (
          <tr key={item._id}>
            {columns.map((column) => (
              <td key={createKey(item, column)}>{renderCell(item, column)}</td>
            ))}
          </tr>
        ))
      )}
    </tbody>
  );
};

TableBody.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  items: PropTypes.array.isRequired,
};

export default TableBody;
