import "./App.css";
import Movies from "./components/Movies";

function App() {
  return (
    <main className="container-fluid py-5">
      <Movies />
    </main>
  );
}

export default App;
